
.PHONY: init
init:
	@pip install \
	-r src/requirements.txt \
	-r src/requirements-dev.txt
	@pre-commit install


.PHONY: compile
compile:
	@rm -rf src/requirements*.txt
	@pip-compile src/requirements.in
	@pip-compile src/requirements-dev.in

.PHONY: sync
sync:
	@pip-sync src/requirements*.txt

.PHONY: runserver
runserver:
	@python src/pilot_app/manage.py runserver

.PHONY: test
test:
	@pytest src/pilot_app


.PHONY: migrations
migrations:
	@python src/pilot_app/manage.py makemigrations

.PHONY: migrate
migrate:
	@python src/pilot_app/manage.py migrate


.PHONY: initdata
initdata:
	@python src/pilot_app/manage.py initdata

.PHONY: printusers
printusers:
	@python src/pilot_app/manage.py printusers
