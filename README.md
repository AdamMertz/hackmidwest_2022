# Some app

Our very cool neat awesome app


## Dev

Run the following to start. Assuming you already have a 3.10 virtualenv.

```shell
$ make init
$ make test
```

To run dev server run the following.
```shell
$ make migrate
$ make runserver
$ make initdata  # inserts fake users
```

Run the following to update requirements

```shell
$ make compile
$ make sync
```

Open chrome w/o web security

```shell
open -n -a /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --user-data-dir="/tmp/chrome_dev_sess_1" --disable-web-security
```
