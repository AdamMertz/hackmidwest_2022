import random

from django.core.management import BaseCommand

from pilot_app.search import models
from tests import factories


class Command(BaseCommand):
    def handle(self, *args, **options):
        for _ in range(4):
            details = factories.InstructorDetailsFactory()
            val = random.randint(1, 3)
            if val in {1, 3}:
                details.certifications.add(
                    models.Certification.objects.get_private_certification()
                )
            if val in {2, 3}:
                details.certifications.add(
                    models.Certification.objects.get_commercial_certification()
                )
            details.save()

        for _ in range(4):
            _ = factories.UserFactory()
