from django.core.management import BaseCommand

from pilot_app.search import models, serializers


class Command(BaseCommand):
    def handle(self, *args, **options):
        users = models.User.objects.all()
        for u in users:
            if u.type.id == 2:
                print(serializers.InstructorSerializer(u).data)
            else:
                print(serializers.StudentSerializer(u).data)
