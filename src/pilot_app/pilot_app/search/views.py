from django import shortcuts
from rest_framework.response import Response
from rest_framework.views import APIView

from pilot_app.search import models, serializers


class InstructorView(APIView):
    def get(self, request, user_id: int):
        user = shortcuts.get_object_or_404(models.User, id=user_id)
        return Response(serializers.InstructorSerializer(user).data)


class InstructorList(APIView):
    def get(self, request):
        certification = request.query_params.get("certification")
        city = request.query_params.get("city")
        state = request.query_params.get("state")
        rate_min = request.query_params.get("rate_min")
        rate_max = request.query_params.get("rate_max")

        instructors = models.User.objects.filter(
            type__name=models.UserType.Values.INSTRUCTOR.value
        )

        if certification:
            instructors = instructors.filter(
                details__certifications__name=certification
            )
        if city:
            instructors = instructors.filter(address__city=city)
        if state:
            instructors = instructors.filter(address__state=state)
        if rate_min:
            instructors = instructors.filter(details__rate__gte=rate_min)
        if rate_max:
            instructors = instructors.filter(details__rate__lte=rate_max)

        return Response(
            {"data": serializers.InstructorSerializer(instructors, many=True).data}
        )


class BookingView(APIView):
    def get(self, request, booking_id: int):
        booking = shortcuts.get_object_or_404(models.Booking, id=booking_id)

        return Response(serializers.BookingSerializer(booking).data)

    def put(self, request, booking_id: int):
        booking = shortcuts.get_object_or_404(models.Booking, id=booking_id)

        certified = request.data["certified"]
        booking.certified = certified

        return Response(serializers.BookingSerializer(booking).data)


class BookingList(APIView):
    def get(self, request):
        student_id = request.query_params.get("student_id")
        instructor_id = request.query_params.get("instructor_id")
        certification = request.query_params.get("certification")

        bookings = models.Booking.objects.all()

        if student_id:
            bookings = bookings.filter(student_id=student_id)
        if instructor_id:
            bookings = bookings.filter(instructor_id=instructor_id)
        if certification:
            bookings = bookings.filter(certification__name=certification)

        return Response(
            {"data": serializers.BookingSerializer(bookings, many=True).data}
        )

    def post(self, request):
        booking = models.Booking.objects.create(
            student_id=request.data["student_id"],
            instructor_id=request.data["instructor_id"],
            start_dt=request.data["start_dt"],
            end_dt=request.data["end_dt"],
            certification=models.Certification.objects.get(
                name=request.data["certification"]
            ),
        )
        return Response(serializers.BookingSerializer(booking).data)


class StudentView(APIView):
    def get(self, request, user_id: int):
        student = models.User.objects.get(id=user_id)
        return Response(serializers.StudentSerializer(student).data)


class StudentList(APIView):
    def get(self, request):
        students = models.User.objects.all()

        return Response(
            {"data": serializers.StudentSerializer(students, many=True).data}
        )
