# Generated by Django 4.0.6 on 2022-07-24 00:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("search", "0006_instructordetails"),
    ]

    operations = [
        migrations.AlterField(
            model_name="address",
            name="address_two",
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="instructordetails",
            name="user",
            field=models.OneToOneField(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="details",
                to="search.user",
            ),
        ),
    ]
