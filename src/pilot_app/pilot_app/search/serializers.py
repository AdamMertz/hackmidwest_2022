from rest_framework import serializers

from pilot_app.search import models


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Address
        fields = ("address_one", "address_two", "city", "state", "zip_code")


class UserTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserType
        fields = "__all__"


class CertificationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Certification
        fields = ("name",)


class InstructorDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.InstructorDetails
        fields = ("rate", "flight_hours", "certifications")

    certifications = serializers.ListSerializer(child=CertificationsSerializer())


class InstructorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "type",
            "address",
            "details",
            "profile",
        )

    type = UserTypeSerializer()
    address = AddressSerializer()
    details = InstructorDetailsSerializer()


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "type",
            "address",
            "profile",
            "certifications",
        )

    type = UserTypeSerializer()
    address = AddressSerializer()
    certifications = serializers.SerializerMethodField()

    def get_certifications(self, value):

        private_hours = float(
            sum(
                (booking.end_dt - booking.start_dt).seconds / 3600
                for booking in value.student_bookings.filter(
                    certification=models.Certification.objects.get_private_certification(),
                    certified=True,
                )
            )
        )
        commercial_hours = float(
            sum(
                (booking.end_dt - booking.start_dt).seconds / 3600
                for booking in value.student_bookings.filter(
                    certification=models.Certification.objects.get_commercial_certification(),
                    certified=True,
                )
            )
        )

        data = [
            {"name": "Private Pilot", "hours": private_hours},
            {"name": "Commercial", "hours": commercial_hours},
        ]
        return data


class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Booking
        fields = (
            "id",
            "student_id",
            "instructor_id",
            "start_dt",
            "end_dt",
            "certification",
            "certified",
        )

    certification = serializers.CharField(source="certification.name")
    student_id = serializers.IntegerField(source="student.id")
    instructor_id = serializers.IntegerField(source="instructor.id")
