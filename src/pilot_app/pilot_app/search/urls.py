from django.urls import path

from pilot_app.search import views

urlpatterns = [
    path("instructor/<int:user_id>", views.InstructorView.as_view(), name="instructor"),
    path("instructors", views.InstructorList.as_view(), name="instructor_list"),
    path("booking/<int:booking_id>", views.BookingView.as_view(), name="booking"),
    path("bookings", views.BookingList.as_view(), name="booking_list"),
    path("student/<int:user_id>", views.StudentView.as_view(), name="student"),
    path("students", views.StudentList.as_view(), name="students"),
]
