import enum

from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models


class UserType(models.Model):
    name = models.CharField(max_length=120)

    class Values(enum.Enum):
        STUDENT = "student"
        INSTRUCTOR = "instructor"


class User(AbstractBaseUser):
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    email = models.EmailField(unique=True)
    type = models.ForeignKey(UserType, on_delete=models.CASCADE)
    address = models.ForeignKey("Address", on_delete=models.CASCADE, null=True)
    profile = models.TextField(default="")

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["email", "first_name", "last_name"]


class Address(models.Model):
    address_one = models.CharField(max_length=255)
    address_two = models.CharField(max_length=255, null=True)
    city = models.CharField(max_length=255)
    state = models.CharField(max_length=10)
    zip_code = models.CharField(max_length=10)


class InstructorDetails(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="details")
    rate = models.IntegerField()
    flight_hours = models.IntegerField()
    certifications = models.ManyToManyField("Certification")


class CertificationManager(models.Manager):
    def get_private_certification(self):
        return self.get(name=Certification.Values.PRIVATE.value)

    def get_commercial_certification(self):
        return self.get(name=Certification.Values.COMMERCIAL.value)


class Certification(models.Model):
    name = models.CharField(max_length=255)

    class Values(enum.Enum):
        PRIVATE = "Private Pilot"
        COMMERCIAL = "Commercial"

    objects = CertificationManager()


class Booking(models.Model):
    student = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="student_bookings"
    )
    instructor = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="instructor_bookings"
    )
    certification = models.ForeignKey(Certification, on_delete=models.CASCADE)
    start_dt = models.DateTimeField()
    end_dt = models.DateTimeField()
    certified = models.BooleanField(default=False)
