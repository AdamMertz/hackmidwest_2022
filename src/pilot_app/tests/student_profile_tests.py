import datetime

import pytest
import pytz
import schema
from django.urls import reverse

from pilot_app.search import models

pytestmark = pytest.mark.usefixtures("db")


_STUDENT_SCHEMA_DICT = {
    "id": int,
    "first_name": str,
    "last_name": str,
    "email": str,
    "type": {"id": int, "name": str},
    "address": schema.Or(
        {
            "address_one": str,
            "address_two": schema.Or(str, None),
            "city": str,
            "state": str,
            "zip_code": str,
        },
        None,
    ),
    "profile": str,
    "certifications": [
        {"name": "Private Pilot", "hours": float},
        {"name": "Commercial", "hours": float},
    ],
}

STUDENT_SCHEMA = schema.Schema(_STUDENT_SCHEMA_DICT)
STUDENT_LIST_SCHEMA = schema.Schema({"data": [_STUDENT_SCHEMA_DICT]})


def test_student_profile_found_returns_200(client, student_factory):
    student = student_factory()

    response = client.get(reverse("search:student", kwargs={"user_id": student.id}))

    assert response.status_code == 200


def test_student_profile_returns_valid_schema(
    client, student_factory, instructor_factory
):
    student = student_factory()

    instructor = instructor_factory()

    now = datetime.datetime.now(tz=pytz.UTC)

    _ = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        start_dt=now,
        end_dt=(now + datetime.timedelta(hours=1)),
        certification=models.Certification.objects.get_private_certification(),
    )

    _ = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        start_dt=now,
        end_dt=(now + datetime.timedelta(hours=1)),
        certification=models.Certification.objects.get_commercial_certification(),
    )

    response = client.get(reverse("search:student", kwargs={"user_id": student.id}))

    STUDENT_SCHEMA.validate(response.json())


def test_student_profile_returns_student_id(client, student_factory):
    _ = student_factory()
    student = student_factory()

    response = client.get(reverse("search:student", kwargs={"user_id": student.id}))

    assert response.json()["id"] == student.id


def test_student_profile_returns_first_name_of_student(client, student_factory):
    student = student_factory()

    response = client.get(reverse("search:student", kwargs={"user_id": student.id}))

    assert response.json()["first_name"] == student.first_name


def test_student_profile_returns_last_name_of_student(client, student_factory):
    student = student_factory()

    response = client.get(reverse("search:student", kwargs={"user_id": student.id}))

    assert response.json()["last_name"] == student.last_name


def test_student_profile_returns_email_of_student(client, student_factory):
    student = student_factory()

    response = client.get(reverse("search:student", kwargs={"user_id": student.id}))

    assert response.json()["email"] == student.email


def test_student_profile_returns_student_type(client, student_factory):
    student = student_factory()

    response = client.get(reverse("search:student", kwargs={"user_id": student.id}))

    assert response.json()["type"] == {"id": student.type.id, "name": student.type.name}


def test_student_profile_returns_address_of_student(client, student_factory):
    student = student_factory()

    response = client.get(reverse("search:student", kwargs={"user_id": student.id}))

    assert response.json()["address"] == {
        "address_one": student.address.address_one,
        "address_two": student.address.address_two,
        "city": student.address.city,
        "state": student.address.state,
        "zip_code": student.address.zip_code,
    }


def test_student_profile_returns_profile_of_student(client, student_factory):
    student = student_factory()
    student.profile = "hello world"
    student.save()

    response = client.get(reverse("search:student", kwargs={"user_id": student.id}))

    assert response.json()["profile"] == student.profile


def test_student_profile_returns_student_certified_private_pilot_hours(
    client, student_factory, instructor_factory
):
    student = student_factory()

    instructor = instructor_factory()

    now = datetime.datetime.now(tz=pytz.UTC)

    _ = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        start_dt=now,
        end_dt=(now + datetime.timedelta(hours=1)),
        certification=models.Certification.objects.get_private_certification(),
    )

    _ = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        start_dt=now,
        end_dt=(now + datetime.timedelta(hours=1)),
        certification=models.Certification.objects.get_private_certification(),
        certified=True,
    )

    response = client.get(reverse("search:student", kwargs={"user_id": student.id}))

    assert response.json()["certifications"] == [
        {"name": "Private Pilot", "hours": 1.0},
        {"name": "Commercial", "hours": 0},
    ]


def test_student_profile_returns_certified_student_commercial_pilot_hours(
    client, student_factory, instructor_factory
):
    student = student_factory()
    instructor = instructor_factory()

    now = datetime.datetime.now(tz=pytz.UTC)

    _ = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        start_dt=now,
        end_dt=(now + datetime.timedelta(hours=1)),
        certification=models.Certification.objects.get_commercial_certification(),
    )

    _ = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        start_dt=now,
        end_dt=(now + datetime.timedelta(hours=1)),
        certification=models.Certification.objects.get_commercial_certification(),
        certified=True,
    )

    response = client.get(reverse("search:student", kwargs={"user_id": student.id}))

    assert response.json()["certifications"] == [
        {"name": "Private Pilot", "hours": 0},
        {"name": "Commercial", "hours": 1.0},
    ]


def test_student_list_returns_200(client):
    response = client.get(reverse("search:students"))

    assert response.status_code == 200


def test_student_list_returns_valid_schema(client, student_factory):
    _ = student_factory()
    response = client.get(reverse("search:students"))

    STUDENT_LIST_SCHEMA.validate(response.json())
