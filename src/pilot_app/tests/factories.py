import factory

from pilot_app.search import models


class AddressFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Address

    address_one = factory.Faker("street_address")
    address_two = None
    city = factory.Faker("city")
    state = factory.Faker("state")
    zip_code = factory.Faker("postcode")


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.User

    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    email = factory.Faker("email")
    address = factory.SubFactory(AddressFactory)
    type_id = 1


class InstructorDetailsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.InstructorDetails

    user = factory.SubFactory(UserFactory, type_id=2)
    rate = factory.Faker("pyint", min_value=10)
    flight_hours = factory.Faker("pyint", min_value=10)
