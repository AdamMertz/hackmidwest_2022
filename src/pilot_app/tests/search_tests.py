import pytest
import schema
from django.urls import reverse

from pilot_app.search import models

pytestmark = pytest.mark.usefixtures("db")

_INSTRUCTOR_SCHEMA_DICT = {
    "id": int,
    "first_name": str,
    "last_name": str,
    "email": str,
    "type": {"id": int, "name": str},
    "address": schema.Or(
        {
            "address_one": str,
            "address_two": schema.Or(str, None),
            "city": str,
            "state": str,
            "zip_code": str,
        },
        None,
    ),
    "details": {"rate": int, "flight_hours": int, "certifications": [{"name": str}]},
    "profile": str,
}


INSTRUCTOR_VIEW_SCHEMA = schema.Schema(_INSTRUCTOR_SCHEMA_DICT)

INSTRUCTOR_LIST_SCHEMA = schema.Schema({"data": [_INSTRUCTOR_SCHEMA_DICT]})


def test_instructor_view_returns_a_404_when_not_found(client):
    response = client.get(reverse("search:instructor", kwargs={"user_id": 1}))

    assert response.status_code == 404


def test_instructor_view_returns_a_200_when_found(client, instructor_factory):
    user = instructor_factory([models.Certification.Values.PRIVATE])

    response = client.get(reverse("search:instructor", kwargs={"user_id": user.id}))

    assert response.status_code == 200


def test_instructor_view_returns_valid_instructor_schema_with_address(
    client, instructor_factory
):
    user = instructor_factory([models.Certification.Values.PRIVATE])

    response = client.get(reverse("search:instructor", kwargs={"user_id": user.id}))

    INSTRUCTOR_VIEW_SCHEMA.validate(response.json())


def test_instructor_view_returns_valid_instructor_schema_with_instructor_details(
    client, instructor_factory
):
    user = instructor_factory([models.Certification.Values.PRIVATE])

    response = client.get(reverse("search:instructor", kwargs={"user_id": user.id}))

    INSTRUCTOR_VIEW_SCHEMA.validate(response.json())


def test_instructor_view_returns_certification_correctly(client, instructor_factory):
    user = instructor_factory([models.Certification.Values.PRIVATE])

    response = client.get(reverse("search:instructor", kwargs={"user_id": user.id}))

    data = response.json()

    certs = data["details"]["certifications"]

    assert len(certs) == 1
    assert certs[0]["name"] == "Private Pilot"


def test_instructors_list_no_instructors_returns_200(client):

    response = client.get(reverse("search:instructor_list"))

    assert response.status_code == 200


def test_instructors_list_no_instructors_returns_empty_list(client):

    response = client.get(reverse("search:instructor_list"))

    assert response.json() == {"data": []}


def test_instructors_list_valid_instructors_schema(client, instructor_factory):

    _ = instructor_factory([models.Certification.Values.PRIVATE])

    _ = instructor_factory(
        [models.Certification.Values.PRIVATE, models.Certification.Values.COMMERCIAL]
    )

    response = client.get(reverse("search:instructor_list"))

    INSTRUCTOR_LIST_SCHEMA.validate(response.json())


def test_instructors_list_can_filter_by_certification_type(client, instructor_factory):
    _ = instructor_factory([models.Certification.Values.PRIVATE])

    _ = instructor_factory(
        [models.Certification.Values.PRIVATE, models.Certification.Values.COMMERCIAL]
    )

    response = client.get(
        reverse("search:instructor_list") + "?certification=Commercial"
    )

    data = response.json()["data"]

    assert len(data) == 1
    assert any(c["name"] == "Commercial" for c in data[0]["details"]["certifications"])


def test_instructors_list_can_filter_by_instructor_city(client, instructor_factory):
    instructor = instructor_factory([models.Certification.Values.PRIVATE])

    _ = instructor_factory(
        [models.Certification.Values.PRIVATE, models.Certification.Values.COMMERCIAL]
    )

    response = client.get(
        reverse("search:instructor_list") + f"?city={instructor.address.city}"
    )

    data = response.json()["data"]

    assert len(data) == 1
    assert data[0]["address"]["city"] == instructor.address.city


def test_instructors_list_can_filter_by_instructor_state(client, instructor_factory):
    instructor = instructor_factory([models.Certification.Values.PRIVATE])

    _ = instructor_factory(
        [models.Certification.Values.PRIVATE, models.Certification.Values.COMMERCIAL]
    )

    response = client.get(
        reverse("search:instructor_list") + f"?state={instructor.address.state}"
    )

    data = response.json()["data"]

    assert len(data) == 1
    assert data[0]["address"]["state"] == instructor.address.state


def test_instructors_list_can_filter_by_instructor_rate_min(client, instructor_factory):
    _ = instructor_factory([models.Certification.Values.PRIVATE], rate=200)

    _ = instructor_factory(
        [models.Certification.Values.PRIVATE, models.Certification.Values.COMMERCIAL],
        rate=100,
    )

    response = client.get(reverse("search:instructor_list") + "?rate_min=150")

    data = response.json()["data"]

    assert len(data) == 1
    assert data[0]["details"]["rate"] >= 150


def test_instructors_list_can_filter_by_instructor_rate_max(client, instructor_factory):
    _ = instructor_factory([models.Certification.Values.PRIVATE], rate=200)

    _ = instructor_factory(
        [models.Certification.Values.PRIVATE, models.Certification.Values.COMMERCIAL],
        rate=100,
    )

    response = client.get(reverse("search:instructor_list") + "?rate_max=150")

    data = response.json()["data"]

    assert len(data) == 1
    assert data[0]["details"]["rate"] <= 150
