import datetime

import pytest
import pytz
import schema
from django.urls import reverse
from rest_framework.test import APIClient

from pilot_app.search import models


pytestmark = pytest.mark.usefixtures("db")

_BOOKING_SCHEMA_DICT = {
    "id": int,
    "student_id": int,
    "instructor_id": int,
    "start_dt": str,
    "end_dt": str,
    "certification": str,
    "certified": bool,
}

BOOKING_SCHEMA = schema.Schema(_BOOKING_SCHEMA_DICT)

BOOKING_LIST_SCHEMA = schema.Schema({"data": [_BOOKING_SCHEMA_DICT]})


def test_get_booking_not_found_returns_404(client):
    response = client.get(reverse("search:booking", kwargs={"booking_id": 2}))

    assert response.status_code == 404


def test_get_booking_valid_request_returns_200(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    response = client.get(reverse("search:booking", kwargs={"booking_id": booking.id}))

    assert response.status_code == 200


def test_get_booking_returns_booking_id(client, instructor_factory, student_factory):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    booking2 = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    response = client.get(reverse("search:booking", kwargs={"booking_id": booking2.id}))

    assert response.json()["id"] == booking2.id


def test_get_booking_returns_certification(client, instructor_factory, student_factory):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    booking2 = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_commercial_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    response = client.get(reverse("search:booking", kwargs={"booking_id": booking2.id}))

    assert response.json()["certification"] == booking2.certification.name


def test_get_booking_returns_target_student_id(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    response = client.get(reverse("search:booking", kwargs={"booking_id": booking.id}))

    assert response.json()["student_id"] == student.id


def test_get_booking_returns_target_instructor_id(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    response = client.get(reverse("search:booking", kwargs={"booking_id": booking.id}))

    assert response.json()["instructor_id"] == instructor.id


def test_get_booking_returns_start_dt(client, instructor_factory, student_factory):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )
    booking.refresh_from_db()

    response = client.get(reverse("search:booking", kwargs={"booking_id": booking.id}))

    assert response.json()["start_dt"] == booking.start_dt.isoformat().replace(
        "+00:00", "Z"
    )


def test_get_booking_returns_end_dt(client, instructor_factory, student_factory):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )
    booking.refresh_from_db()

    response = client.get(reverse("search:booking", kwargs={"booking_id": booking.id}))

    assert response.json()["end_dt"] == booking.end_dt.isoformat().replace(
        "+00:00", "Z"
    )


def test_get_booking_valid_schema(client, instructor_factory, student_factory):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    response = client.get(reverse("search:booking", kwargs={"booking_id": booking.id}))

    BOOKING_SCHEMA.validate(response.json())


def test_get_booking_list_valid_request_returns_200(client):
    response = client.get(reverse("search:booking_list"))

    assert response.status_code == 200


def test_booking_list_returns_empty_list_on_no_data(client):
    response = client.get(reverse("search:booking_list"))

    assert response.json() == {"data": []}


def test_booking_list_returns_valid_schema(client, instructor_factory, student_factory):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    response = client.get(reverse("search:booking_list"))

    BOOKING_LIST_SCHEMA.validate(response.json())


def test_booking_list_returns_users_bookings(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    response = client.get(reverse("search:booking_list"))

    data = response.json()

    assert len(data["data"]) == 1


def test_booking_list_can_filter_by_student_id_for_bookings(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    student2 = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    _ = models.Booking.objects.create(
        student=student2,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    response = client.get(reverse("search:booking_list") + f"?student_id={student.id}")

    data = response.json()

    assert len(data["data"]) == 1
    assert data["data"][0]["student_id"] == booking.student.id


def test_booking_list_can_filter_by_instructor_id_for_bookings(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    instructor2 = instructor_factory()
    student = student_factory()

    _ = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    _ = models.Booking.objects.create(
        student=student,
        instructor=instructor2,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    response = client.get(
        reverse("search:booking_list") + f"?instructor_id={instructor.id}"
    )

    data = response.json()

    assert len(data["data"]) == 1
    assert data["data"][0]["instructor_id"] == instructor.id


def test_booking_list_can_filter_by_certification_name(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    _ = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_private_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get_commercial_certification(),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    response = client.get(reverse("search:booking_list") + f"?certification=Commercial")

    data = response.json()

    assert len(data["data"]) == 1
    assert data["data"][0]["certification"] == booking.certification.name


def test_creating_valid_booking_returns_200(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking_data = {
        "student_id": student.id,
        "instructor_id": instructor.id,
        "start_dt": datetime.datetime.utcnow(),
        "end_dt": datetime.datetime.utcnow() + datetime.timedelta(hours=1),
        "certification": "Private Pilot",
    }

    response = client.post(reverse("search:booking_list"), data=booking_data)

    assert response.status_code == 200


def test_creating_valid_booking_returns_booking_entity(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking_data = {
        "student_id": student.id,
        "instructor_id": instructor.id,
        "start_dt": datetime.datetime.utcnow(),
        "end_dt": datetime.datetime.utcnow() + datetime.timedelta(hours=1),
        "certification": "Private Pilot",
    }

    response = client.post(reverse("search:booking_list"), data=booking_data)

    BOOKING_SCHEMA.validate(response.json())


def test_creating_valid_booking_returns_start_dt(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking_data = {
        "student_id": student.id,
        "instructor_id": instructor.id,
        "start_dt": datetime.datetime.now(tz=pytz.UTC),
        "end_dt": datetime.datetime.now(tz=pytz.UTC) + datetime.timedelta(hours=1),
        "certification": "Private Pilot",
    }

    response = client.post(reverse("search:booking_list"), data=booking_data)

    data = response.json()

    booking = models.Booking.objects.get(pk=data["id"])

    assert data["start_dt"] == str(booking.start_dt)


def test_creating_valid_booking_returns_end_dt(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking_data = {
        "student_id": student.id,
        "instructor_id": instructor.id,
        "start_dt": datetime.datetime.now(tz=pytz.UTC),
        "end_dt": datetime.datetime.now(tz=pytz.UTC) + datetime.timedelta(hours=1),
        "certification": "Private Pilot",
    }

    response = client.post(reverse("search:booking_list"), data=booking_data)

    data = response.json()

    booking = models.Booking.objects.get(pk=data["id"])

    assert data["end_dt"] == str(booking.end_dt)


def test_creating_valid_booking_returns_student_user_id(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking_data = {
        "student_id": student.id,
        "instructor_id": instructor.id,
        "start_dt": datetime.datetime.utcnow(),
        "end_dt": datetime.datetime.utcnow() + datetime.timedelta(hours=1),
        "certification": "Private Pilot",
    }

    response = client.post(reverse("search:booking_list"), data=booking_data)

    data = response.json()

    booking = models.Booking.objects.get(pk=data["id"])

    assert data["student_id"] == booking.student_id


def test_creating_valid_booking_returns_instructor_user_id(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking_data = {
        "student_id": student.id,
        "instructor_id": instructor.id,
        "start_dt": datetime.datetime.utcnow(),
        "end_dt": datetime.datetime.utcnow() + datetime.timedelta(hours=1),
        "certification": "Private Pilot",
    }

    response = client.post(reverse("search:booking_list"), data=booking_data)

    data = response.json()

    booking = models.Booking.objects.get(pk=data["id"])

    assert data["instructor_id"] == booking.instructor_id


def test_creating_valid_booking_returns_certification(
    client, instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking_data = {
        "student_id": student.id,
        "instructor_id": instructor.id,
        "start_dt": datetime.datetime.utcnow(),
        "end_dt": datetime.datetime.utcnow() + datetime.timedelta(hours=1),
        "certification": "Commercial",
    }

    response = client.post(reverse("search:booking_list"), data=booking_data)

    data = response.json()

    booking = models.Booking.objects.get(pk=data["id"])

    assert data["certification"] == booking.certification.name


def test_update_booking_returns_200_on_valid_booking(
    instructor_factory, student_factory
):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get(name="Commercial"),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    client = APIClient()

    put_data = {"certified": True}

    response = client.put(
        reverse("search:booking", kwargs={"booking_id": booking.id}), data=put_data
    )

    assert response.status_code == 200


def test_update_booking_returns_404_on_valid_booking(client):

    response = client.put(reverse("search:booking", kwargs={"booking_id": 1}))

    assert response.status_code == 404


def test_update_booking_returns_can_certify(instructor_factory, student_factory):
    instructor = instructor_factory()
    student = student_factory()

    booking = models.Booking.objects.create(
        student=student,
        instructor=instructor,
        certification=models.Certification.objects.get(name="Commercial"),
        start_dt=datetime.datetime.utcnow(),
        end_dt=datetime.datetime.utcnow(),
    )

    client = APIClient()

    put_data = {"certified": True}

    response = client.put(
        reverse("search:booking", kwargs={"booking_id": booking.id}), data=put_data
    )

    data = response.json()
    assert data["certified"] is True
