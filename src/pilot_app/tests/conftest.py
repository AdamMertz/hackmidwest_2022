import pytest
import typing

from pilot_app.search import models
from tests import factories


@pytest.fixture()
def instructor_factory():
    def _factory(
        certifications: typing.Optional[
            typing.List[typing.Union[str, models.Certification.Values]]
        ] = None,
        rate: typing.Optional[int] = 120,
    ):
        certifications = certifications or []

        det = factories.InstructorDetailsFactory(rate=rate)

        for cert in certifications:
            if isinstance(cert, models.Certification.Values):
                cert = cert.value
            det.certifications.add(models.Certification.objects.get(name=cert))

        det.save()

        return det.user

    return _factory


@pytest.fixture()
def student_factory():
    def _factory():
        student = factories.UserFactory(type_id=1)
        return student

    return _factory
