import { Box } from "@mui/material";
import { InstructorResult } from "./InstructorResult";

export const SearchList = ({ instructors }) => {
  return (
    <Box>
      {instructors.map((instructor) => (
        <InstructorResult {...instructor} key={instructor.id} />
      ))}
    </Box>
  );
};
