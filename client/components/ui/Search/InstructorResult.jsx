/* eslint-disable react/react-in-jsx-scope */
import {
  Avatar,
  Button,
  Card,
  CardActions,
  CardContent,
  Chip,
  Grid,
  Typography,
} from "@mui/material";
import { useRouter } from "next/router";

export const InstructorResult = ({
  id,
  first_name,
  last_name,
  address,
  details,
}) => {

  const router = useRouter()

  const handleBookingRoute = (id) => {
    router.push({
      pathname: '/book',
      query: {
        id
      }
    })
  }
  return (
    <Card sx={{ borderRadius: 0, marginBottom: 2 }}>
      <CardContent>
        <Grid
          container
          direction="row"
          justifyContent="flex-start"
          alignItems="center"
          wrap="nowrap"
          spacing={2}
        >
          <Grid item>
            <Avatar
              variant="square"
              sx={{ width: 100, height: 100, backgroundColor: "#303030" }}
              src={`https://avatars.dicebear.com/api/avataaars/${first_name}${last_name}.svg`}
            />
          </Grid>
          <Grid item>
            <Typography
              variant="h5"
              color="text.secondary"
              sx={{ display: "flex", width: "100%", justifyContent: "space-between" }}
            >
              <span>
                {first_name} {last_name}
              </span>
              <span style={{ display: "block", textAlign: "right" }}>
                ${details.rate}/hr
              </span>
            </Typography>
            <Typography variant="b5" color="text.secondary">
              {address.city}, {address.state}
              {"     "}
              {details.certifications.map((l, index) => (
                <Chip
                  size="small"
                  key={index}
                  label={l.name}
                  sx={{ margin: "0 5px", textTransform: "uppercase" }}
                />
              ))}
            </Typography>
            <br />
            <Typography variant="p5" color="text.secondary">
              Lorem ipsum dolor sit amet consectetur, adipisicing elit.
              Inventore eos modi necessitatibus cupiditate reiciendis optio
              harum veritatis omnis minima, quasi doloremque animi quidem fugit
              repellat recusandae ab officiis odio architecto.
            </Typography>
            <Button
              variant="contained"
              sx={{ marginTop: 2, display: "flex" }}
              onClick={() => handleBookingRoute(id)}
            >
              Book Now
            </Button>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};
