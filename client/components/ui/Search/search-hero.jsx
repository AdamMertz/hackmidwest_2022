import {
  Paper,
  Typography,
  Grid,
  Box,
  TextField,
  MenuItem,
  Button,
} from "@mui/material";
import Clouds from "../../../public/clouds.jpg";
import { useState } from "react";
import { useRouter } from "next/router";
import { useEffect } from "react";
import states from "../../../lib/states.json";

const certifications = [
  { label: "Private license", value: "Private Pilot" },
  { label: "Commercial license", value: "Commercial" },
];

export const SearchHero = () => {
  const router = useRouter();
  const [certification, setCertification] = useState("");
  const [city, setCity] = useState("");
  const [state, setState] = useState("");
  const [minPrice, setMinPrice] = useState("");
  const [maxPrice, setMaxPrice] = useState("");

  useEffect(() => {
    const { city, cert, state, min, max } = router.query;

    if (city || cert || state || min || max) {
      city ? setCity(city) : false;
      cert ? setCertification(cert) : false;
      state ? setState(state) : false;
      min ? setMinPrice(min) : false;
      max ? setMaxPrice(max) : false;
    }
  }, []);

  const handleQueryRoute = () => {
    router.push({
      pathname: "/search",
      query: {
        city,
        cert: certification,
        state,
        min: minPrice,
        max: maxPrice,
      },
    });
  };

  return (
    <Paper
      elevation={0}
      sx={{
        backgroundImage: `linear-gradient(rgba(255,255,255,1), rgba(255,255,255,.2)), url(${Clouds.src})`,
        marginBottom: 2,
        padding: "0 10px",
      }}
    >
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
      >
        <Grid item xs={12} my={8}>
          <Typography align="center" variant="h2" component="h1">
            Take to the sky.
          </Typography>
          <Box component="form" mt={4}>
            <TextField
              id="certification-type"
              select
              value={certification}
              onChange={(event) => setCertification(event.target.value)}
              label="Certification Type"
              sx={{ width: "100%", marginBottom: 2, backgroundColor: "white" }}
            >
              {certifications.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              id="city"
              label="City"
              variant="outlined"
              value={city}
              onChange={(event) => setCity(event.target.value)}
              sx={{ width: "100%", marginBottom: 2, backgroundColor: "white" }}
            ></TextField>
            <TextField
              id="state"
              select
              value={state}
              onChange={(event) => setState(event.target.value)}
              label="State"
              sx={{ width: "100%", marginBottom: 2, backgroundColor: "white" }}
            >
              {states.map((option) => (
                <MenuItem key={option.abbreviation} value={option.name}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
            <div>
              <TextField
                id="min-range"
                type="number"
                label="Min $"
                variant="outlined"
                value={minPrice}
                onChange={(event) => setMinPrice(event.target.value)}
                sx={{
                  margin: 1,
                  marginLeft: 0,
                  backgroundColor: "white",
                  width: "48%",
                }}
              />
              <TextField
                id="max-range"
                type="number"
                label="Max $"
                variant="outlined"
                value={maxPrice}
                onChange={(event) => setMaxPrice(event.target.value)}
                sx={{
                  margin: 1,
                  marginRight: 0,
                  backgroundColor: "white",
                  width: "48%",
                }}
              />
            </div>
            <Button
              variant="contained"
              sx={{ marginLeft: "auto", display: "flex", marginTop: 2 }}
              onClick={handleQueryRoute}
            >
              Find Pilots Now
            </Button>
          </Box>
        </Grid>
      </Grid>
    </Paper>
  );
};
