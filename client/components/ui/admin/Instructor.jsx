import { useState } from "react";
import {
  Box,
  TextField,
  FormGroup,
  FormControlLabel,
  Checkbox,
  FormLabel,
  Button,
} from "@mui/material";

export const InstructorForm = () => {
  const [bio, setBio] = useState();
  const [rate, setRate] = useState();
  const [ppl, setPpl] = useState(false);
  const [cpl, setCpl] = useState(false);

  return (
    <Box maxWidth="sm" sx={{ margin: "20px auto" }}>
      <TextField
        id="bio"
        label="Bio"
        multiline
        value={bio}
        onChange={(event) => setBio(event.target.value)}
        variant="outlined"
        sx={{ width: "100%", marginBottom: 2 }}
      />
      <TextField
        id="rate"
        label="Rate"
        type="number"
        value={rate}
        onChange={(event) => setRate(event.target.value)}
        variant="outlined"
        sx={{ width: "100%", marginBottom: 2 }}
      />
      <FormGroup>
        <FormLabel component="legend">Certifications</FormLabel>
        <FormControlLabel
          control={
            <Checkbox
              checked={ppl}
              onChange={(event) => setPpl(event.target.checked)}
              name="ppl"
            />
          }
          label="PPL"
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={cpl}
              onChange={(event) => setCpl(event.target.checked)}
              name="cpl"
            />
          }
          label="CPL"
        />
      </FormGroup>
      <Button
        variant="contained"
        sx={{ margin: "20px auto 0", display: "flex" }}
      >
        Save Settings
      </Button>
    </Box>
  );
};
