import { useState } from "react";
import {
  Box,
  TextField,
  FormGroup,
  FormControlLabel,
  Checkbox,
  FormLabel,
  Button,
} from "@mui/material";

export const StudentForm = () => {
  const [ppl, setPpl] = useState("");
  const [cpl, setCpl] = useState("");

  return (
    <Box maxWidth="sm" sx={{ margin: "20px auto" }}>
      <FormGroup>
        <FormLabel component="legend">Certifications</FormLabel>
        <FormControlLabel
          control={
            <Checkbox
              checked={ppl}
              onChange={(event) => setPpl(event.target.checked)}
              name="ppl"
            />
          }
          label="PPL"
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={cpl}
              onChange={(event) => setCpl(event.target.checked)}
              name="cpl"
            />
          }
          label="CPL"
        />
      </FormGroup>
      <Button
        variant="contained"
        sx={{ margin: "20px auto 0", display: "flex" }}
      >
        Save Settings
      </Button>
    </Box>
  );
};
