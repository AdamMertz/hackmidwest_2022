import { BookingResult } from "./BookingResult";

const times = [
  "8:00",
  "9:00",
  "10:00",
  "11:00",
  "12:00",
  "1:00",
  "2:00",
  "3:00",
  "4:00",
  "5:00",
  "6:00",
];

const options = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric",
};

const dates = [
  new Date("7/24/2022").toLocaleDateString("en-US", options),
  new Date("7/25/2022").toLocaleDateString("en-US", options),
  new Date("7/26/2022").toLocaleDateString("en-US", options),
  new Date("7/27/2022").toLocaleDateString("en-US", options),
  new Date("7/28/2022").toLocaleDateString("en-US", options),
  new Date("7/29/2022").toLocaleDateString("en-US", options),
  new Date("7/30/2022").toLocaleDateString("en-US", options),
];

export const BookingList = ({certifications, id}) => {
  return (
    <>
      {dates.map((date, index) => (
        <BookingResult times={times} date={date} certifications={certifications} key={index} id={id} />
      ))}
    </>
  );
};
