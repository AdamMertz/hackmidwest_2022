import {
  Typography,
  Grid,
  Chip,
  Card,
  CardContent,
  Modal,
  Button,
  TextField,
  MenuItem,
  Box,
} from "@mui/material";
import { useRouter } from "next/router";
import { useState } from "react";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export const BookingResult = ({ date, times, certifications, id }) => {
  const router = useRouter()
  const [open, setOpen] = useState(false);
  const [activeDate, setActiveDate] = useState("");
  const [activeTime, setActiveTime] = useState("");
  const [certification, setCertification] = useState("");

  const handleOpen = (date, time) => {
    setActiveDate(date);
    setActiveTime(time);
    setOpen(true);
  };
  const handleClose = () => setOpen(false);

  const handleSubmit = async () => {
    const start_date = new Date(`${activeDate} ${activeTime}:00`).toISOString();
    const end_date_plus =
      new Date(`${activeDate} ${activeTime}:00`).getTime() + 1 * 60 * 60 * 1000;
    const end_date = new Date(end_date_plus).toISOString();

console.log(certification.toLowerCase())
    try {
      const response = await fetch("http://127.0.0.1:8000/search/bookings", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          student_id: 6,
          instructor_id: id,
          certification,
          start_dt: start_date,
          end_dt: end_date,
        }),
      });

      const data = await response.json();

      router.push(`/admin/profile?id=6`)
    } catch (error) {
      console.error(error)
    }
  };

  return (
    <>
      <Card sx={{ borderRadius: 0, marginBottom: 2 }}>
        <CardContent>
          <Grid
            container
            direction="row"
            justifyContent="flex-start"
            alignItems="center"
            wrap="nowrap"
            spacing={2}
          >
            <Grid item>
              <Typography variant="h5" color="text.secondary" mb={2}>
                {date}
              </Typography>
              {times.map((time, index) => (
                <Chip
                  key={index}
                  size="large"
                  label={time}
                  onClick={() => handleOpen(date, time)}
                  sx={{ margin: "0 20px 20px 0", textTransform: "uppercase" }}
                />
              ))}
            </Grid>
          </Grid>
        </CardContent>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Box>
              <Typography variant="h4" color="text.secondary">
                Book this instructor
              </Typography>
              <Typography id="modal-modal-title" variant="body1" component="h2">
                {activeDate} at {activeTime}
              </Typography>
              <TextField
                id="certification-type"
                select
                value={certification}
                onChange={(event) => setCertification(event.target.value)}
                label="Certification Type"
                sx={{
                  width: "100%",
                  marginTop: 3,
                  backgroundColor: "white",
                }}
              >
                {certifications.map((option) => (
                  <MenuItem key={option.name} value={option.name}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
              <Typography mt={2} variant="caption" component="small">
                By confirming this booking you are agreeing to the terms and
                conditions and will be able to provide age verification upon
                request.
              </Typography>
              <Button
                variant="contained"
                sx={{ margin: "20px auto 0", display: "flex" }}
                onClick={handleSubmit}
              >
                Confirm
              </Button>
            </Box>
          </Box>
        </Modal>
      </Card>
    </>
  );
};
