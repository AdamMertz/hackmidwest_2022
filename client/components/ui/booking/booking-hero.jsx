import { Paper, Typography, Grid, Avatar, Chip } from "@mui/material";

export const BookingHero = ({ instructor }) => {
  return (
    <Paper
      elevation={3}
      sx={{
        marginBottom: 2,
      }}
      p={8}
    >
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="center"
        wrap="nowrap"
        spacing={2}
      >
        <Grid item xs={12} my={8} px={2} container justifyContent="center">
          <Avatar
            variant="square"
            sx={{ width: "50%", height: "50%", backgroundColor: "#303030" }}
            src={`https://avatars.dicebear.com/api/avataaars/${instructor.first_name}${instructor.last_name}.svg`}
          />
        </Grid>
        <Grid item px={2}>
          <Typography variant="h3" color="text.secondary">
            {instructor.first_name} {instructor.last_name}
          </Typography>
          <Typography variant="b5" color="text.secondary">
            {instructor.address.city}, {instructor.address.state}
            {"     "}
            {instructor.details.certifications.map((l, index) => (
              <Chip
                size="small"
                key={index}
                label={l.name}
                sx={{ margin: "0 5px", textTransform: "uppercase" }}
              />
            ))}
          </Typography>
          <br />
          <Typography variant="p5" color="text.secondary">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Inventore
            eos modi necessitatibus cupiditate reiciendis optio harum veritatis
            omnis minima, quasi doloremque animi quidem fugit repellat
            recusandae ab officiis odio architecto.
          </Typography>
        </Grid>
      </Grid>
    </Paper>
  );
};
