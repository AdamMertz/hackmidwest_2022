import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import cessna from '../../public/plane.jpg'

export default function FeatureCard(props) {
  const { image, title, text } = props; 

  return (
    <Card>
      <CardMedia
        component="img"
        alt="cessna"
        height="140"
        src={cessna.src}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {text}
        </Typography>
      </CardContent>
      <CardActions>
      </CardActions>
    </Card>
  );
};
