import Box from '@mui/material/Box'
import { SearchHero } from '../components/ui/Search/search-hero'
import Features from '../components/ui/Features'

export default function Index () {
  return (
      <>
        <Box sx={{ my: 4 }}>
          <SearchHero />
        </Box>
        <Features />
      </>
  )
}
