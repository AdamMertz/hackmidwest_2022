import instructors from "../../../_mock/instructor.json";

export default function handler(req, res) {
  const { id } = req.query;

  const instructor = instructors.find((instructor) => instructor.id === id);
  res.status(200).json(instructor);
}
