import instructors from "../../../_mock/instructor.json";

export default function handler(req, res) {
  res.status(200).json(instructors)
}
