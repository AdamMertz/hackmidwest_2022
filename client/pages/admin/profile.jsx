import { Card, CardContent, Typography, Grid } from "@mui/material";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { ProfileHero } from "../../components/ui/admin/profile-hero";

const Profile = ({ student }) => {
  const router = useRouter();
  const [bookings, setBookings] = useState([]);

  const { id } = router.query;
  useEffect(() => {
    const getBookings = async () => {
      const response = await fetch("http://127.0.0.1:8000/search/bookings");
      const json = await response.json();
      const { data } = json;
      const bookingData = data.filter((booking) => {
        console.log(booking);
        return booking.student_id === +id;
      });
      setBookings(bookingData);
    };

    getBookings();
  }, []);

  console.log(bookings);

  const options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "2-digit",
    minute: "numeric",
  };

  const getDate = (date) => {
    return new Date(date).toLocaleDateString("en-US", options);
  };
  return (
    <>
      <ProfileHero client={student} />
      <Typography component="h2" variant="h4" align="center" my={2}>
        Bookings
      </Typography>
      {bookings &&
        bookings.map((booking) => (
          <Card sx={{ borderRadius: 0, marginBottom: 2 }} key={booking.id}>
            <CardContent>
              <Grid
                container
                direction="row"
                justifyContent="flex-start"
                alignItems="center"
                wrap="nowrap"
                spacing={2}
              >
                <Grid item>
                  <Typography variant="h5" color="text.secondary" mb={2}>
                    {getDate(booking.start_dt)}
                  </Typography>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        ))}
    </>
  );
};

export default Profile;

export async function getServerSideProps({ query }) {
  const { id } = query;

  let student;

  try {
    const response = await fetch(`http://127.0.0.1:8000/search/student/${id}`);
    student = await response.json();
  } catch (error) {
    console.error(error);
  }

  return {
    props: {
      student,
    }, // will be passed to the page component as props
  };
}
