import { useRouter } from "next/router";
import { InstructorForm } from "../../components/ui/admin/Instructor";
import { Paper, Grid, Typography } from "@mui/material";
import { StudentForm } from "../../components/ui/admin/student";

const Admin = () => {
  const router = useRouter();

  const { role } = router.query;

  return (
    <>
      {" "}
      <Paper
        elevation={3}
        sx={{
          marginBottom: 2,
        }}
        p={8}
      >
        <Grid
          container
          direction="row"
          justifyContent="flex-start"
          alignItems="center"
          wrap="nowrap"
          spacing={2}
        >
          <Grid item xs={12} my={8} px={2} container justifyContent="center">
            <Typography component="h1" variant="h2">
              Account Settings
            </Typography>
          </Grid>
        </Grid>
      </Paper>
      {role === "instructor" ? <InstructorForm /> : <StudentForm />}
    </>
  );
};

export default Admin;
