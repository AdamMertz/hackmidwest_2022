import { Box, Typography } from "@mui/material";
import { SearchHero } from "../components/ui/Search/search-hero";
import { SearchList } from "../components/ui/Search/SearchList";

const Search = ({ instructors }) => {
  return (
    <Box sx={{ my: 4 }}>
      <SearchHero />
      {instructors && <SearchList instructors={instructors} />}
      {instructors.length === 0 && (
        <Typography component="h2" variant="h5" align="center">
          No pilots found matching this search
        </Typography>
      )}
    </Box>
  );
};

export default Search;

export async function getServerSideProps({ query }) {
  const { city, cert, state, min, max } = query;

  let instructors;

  if (city || cert || state || min || max) {
    try {
      const response = await fetch(
        `http://127.0.0.1:8000/search/instructors?city=${city}&certification=${cert}&state=${state}&rate_min=${min}&rate_max=${max}`
      );
      instructors = await response.json();
    } catch (error) {
      console.error(error);
    }
  } else {
    try {
      const response = await fetch(`http://127.0.0.1:8000/search/instructors`);
      instructors = await response.json();
    } catch (error) {
      console.error(error);
    }
  }

  return {
    props: {
      instructors: instructors.data,
    }, // will be passed to the page component as props
  };
}
