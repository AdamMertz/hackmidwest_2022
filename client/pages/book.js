import { BookingHero } from "../components/ui/booking/booking-hero";
import { BookingList } from "../components/ui/booking/BookingList";

const Book = ({ instructor }) => {
  return (
    <>
      <BookingHero instructor={instructor} />
      <BookingList certifications={instructor.details.certifications} id={instructor.id} />
    </>
  );
};

export default Book;

export async function getServerSideProps({ query }) {
  const { id } = query;

  let instructor;

  try {
    const response = await fetch(
      `http://127.0.0.1:8000/search/instructor/${id}`
    );
    instructor = await response.json();
  } catch (error) {
    console.error(error);
  }

  return {
    props: {
      instructor,
    }, // will be passed to the page component as props
  };
}
