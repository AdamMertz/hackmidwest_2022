import "../styles/globals.css";
import Container from "@mui/material/Container";
import { Grid } from "@mui/material";
import Link from 'next/link';

function MyApp({ Component, pageProps }) {
  return (
    <Container maxWidth="lg">
      <Grid my={2} py={2}>
        <Link href="/">Home</Link>
        <Link href="/search">
          <a style={{marginLeft: "10px"}}>Booking</a>
        </Link>
      </Grid>
      <Component {...pageProps} />
    </Container>
  );
}

export default MyApp;
